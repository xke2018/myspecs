Authentication
---
## login
## logout

- allow user to logout
  - store subscriber logout
  - ffx subscriber logout
  - ffx free user logout
- allow user to logout in any condition (offline)
- resume previous metering and paywall rules after logout

## session retain
