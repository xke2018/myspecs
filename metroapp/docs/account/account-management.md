Account Management
---
## registration
User can use in-app registration flow to create new membership account. 
- if email address already exists, user should see error message
- if email and password are accepted and processed by backend, the user should see toast message and Login/Registration screen will be dismissed
  - if created successfully within 10 seconds, user should see congrats
  - if still processing after 10 seconds, user should see check email later
- if email and password failed to be processed or any backend issue, user should see error message and stay on Registration screen

## reset password