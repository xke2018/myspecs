Metro Ink Mobile App Spec
---
# Introduction
Metro Ink Mobile App is introduced to provide brand new digital reading experience for `Fairfax` subscribers across different news products on both `iOS` and `Android` platform. It's currently accessible for `SMH` and `AGE` and later on will be on for `BT` and `CT`. The app aims to have features that integrated with more services and channels such as `iTunes subscription` and `Google subscription` to expand Fairfax subscription market.

# Screens
The screens:
- Launch Screen: The splash screen represents Fairfax product brand such like `SMH` and `AGE` with landmarked background image and brand logo. On top of it, the app provides ability to detect and promote version upgrade, handle network/service error and retry options.
- FrontPage Screen: This screen is the landing place for readers on app successfully launched. It has a multi-functional header, chapters like Top Stories and Customisable My News empowered by smooth scrolling and swiping.
- Article Screen
- Setting Screen

## Screen Flow
#### Launch Screen -> FrontPage Screen -> Article Screen
<img src="assets/SPLASH.png" alt="drawing" width="25%"/>
<img src="assets/TOPSTORIES_HEADER.png" alt="drawing" width="25%"/>
<img src="assets/ARTICLE_NORMAL.png" alt="drawing" width="25%"/>

#### Launch Screen -> FrontPage Screen -> Setting Screen
<img src="assets/SPLASH.png" alt="drawing" width="25%"/>
<img src="assets/TOPSTORIES_HEADER.png" alt="drawing" width="25%"/>
<img src="assets/SETTINGS.png" alt="drawing" width="25%"/>

#### FrontPage Screen -> MyNews Screen -> MyNews Categories
<span><img src="assets/TOPSTORIES_HEADER_MENU.png" alt="drawing" width="25%"/></span>
<img src="assets/MYNEWS_NORMAL.png" alt="drawing" width="25%"/>
<img src="assets/MYNEWS_CATEGORIES.png" alt="drawing" width="25%"/>

## Launch Screen
- User should see splash screen with background image, logo and app version
- User should see error message and retry button if app fails to fetch data and no offline cache available `error handling`
- User should see app upgrade alert if force/optional upgrade is available `app upgrade`
## FrontPage Screen
- User should see app header and top stories with article listings after app launched
- User can drag or swipe up to dismiss app header
- User can scroll to view article listings within chapters `gesture support`
- User can drag to navigate between chapters `gesture support`
- User can open menu to navigate between chapters
- User can refresh content by tapping on manual refresh button `refresh content`
- User can pull down to refresh content on Top Stories `refresh content`
- User can view and open unsupported articles in default browser by tapping on article `article support`
- User should see onboarding guide the first time visiting chapter MyNews
- User can select or deselect categories in MyNews
- User can reorder categories in MyNews
- User should error state cell if certain category has no article in MyNews `error handling`
- User should see error message and retry button on app header and previous content if app fails to fetch data `refresh content` `error handling`
- User should see embedded Ads in Top Stories `ads`
## Article Screen
- User can view article screen by tapping any article listing in FrontPage Screen
- User can navigate among articles in Article Screen by horizontal swiping `gesture support`
- User can navigate back to FrontPage screen by tapping back button on Article Screen navbar
- User can view different article embeds in Article Screen `article embeds`
- User can view linked articles in default browsers by tapping on link on Article Screen
- User can copy text by long press and select on paragraphs
- User can open share options by tapping share button on Article Screen navbar
- User should see error message on Article Screen if app fails to fetch data `error handling` `refresh content`
- User should see embedded ads per 7 paragraphs in Article Screen `ads`
## Setting Screen
- User can switch between FrontPage Screen and Setting Screen
- User can send feedback on Setting Screen
- User can view Privacy in webview on Setting Screen
- User can view Term of Use in webview on Setting Screen
- User can send email with default device mail client on Setting Screen
- User can view app version information on Setting Screen
# Features
### Gesture support
- Scrolling
  - Scroll within chapter
  - Scroll within article
- Swiping
  - Swipe between articles
- Tapping
  - Tap on article index
  - Tap on article embeds
  - Tap on article external links
- Dragging
  - Drag to jump between chapters
  - Drag to reveal/dismiss header
  - Drag to reorder MyNews categories

### Refresh content
  -	Pull down refresh
  -	Tap to refresh
### Offline reading
  -	Frontpage index offline cache
  -	Article content offline cache
  -	Offline cache purge
### Account
  -	Login
    - Fairfax login
    - Google login
  -	Logout
  -	Forget password
  -	Registration
    - Fairfax
  -	Subscription linking
  -	Account details
### Subscription
  -	Fairfax subscription
  -	iTunes subscription
  -	Google subscription
  -	Unsubscriber
	- Meter prompt
	- Paywall
### Error handling
### App Upgrade
  - Force
  -	Optional
### Article Support
  -	Standard article
  -	Unsupported article
  - Live article
  - Sponsored article

#### Sponsored Article
There are 3 types of Sponsored article: `Sponsored content`, `Partnered content` and `Brought to you`. With those types of articles, article data fetched from `content api` or `asset api` will have additional field `sponsor`. It's legal requirement that Fairfax must label it as `SPONSORED` and add sponsor information in the article.

App needs to consume the label field to show `SPONSORED` which requires editors to follow template
App will show different sponsor information based on sponsorship types as:
|Sponsorship type|Sponsor information to display under article headline| Screen |
|--|--|--|
| sponsor| This is sponsored content for `$sponsorName`| Article Screen |
| partner| This is partnered content in collaboration with `$sponsorName`| Article Screen |
| broughttoyouby| This content is brought to you by `$sponsorName`| Article Screen |
| none| `none` | Article Screen |

> Note:
Metro Ink Web has handled that article label will always be `SPONSORED` if the article has sponsor information. However app team doesn't do this for. Whatever lable has in the article, app will show the label(which requires editor to make sure the template is followed when writing sponsored articles)

### Article Embeds
  - image
  - video
  - iframe
  - facebook
  - twitter
  - infogram
  - instagram
### Ads
  - index ads
  - article ads