iOS Testing
---
# Language
Swift 
- https://swift.org/

# Design Patterns
Page Object
- https://alexilyenko.github.io/xcuitest-page-object/

# Code Style
Swift Style Guide
- https://google.github.io/swift/

# Library
XCTest
- https://developer.apple.com/documentation/xctest

# CI/Build tools
cocoapods
- https://guides.cocoapods.org/

fastlane
- https://docs.fastlane.tools/

buildkite
- https://buildkite.com/docs/tutorials/getting-started

# Mock/Stub tool
Wiremock
- http://wiremock.org/docs/

# Sample Project
https://github.com/Shashikant86/Scalable-XCUItest