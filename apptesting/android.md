Android Testing
---
# Language
Kotlin 
- https://kotlinlang.org/docs/reference/
- https://kotlinlang.org/docs/reference/android-overview.html

# Design Patterns
Robot Pattern
- https://academy.realm.io/posts/kau-jake-wharton-testing-robots/

# Code Style
Kotlin style guide
- https://developer.android.com/kotlin/style-guide

# Library
Espresso
- https://developer.android.com/training/testing/espresso/

Screengrab
- https://docs.fastlane.tools/actions/screengrab/

# CI/Build tools
gradle
- https://developer.android.com/studio/releases/gradle-plugin

fastlane
- https://docs.fastlane.tools/

buildkite
- https://buildkite.com/docs/tutorials/getting-started

# Mock/Stub tool
Wiremock
- http://wiremock.org/docs/
- https://handstandsam.com/2016/01/30/running-wiremock-on-android/

# Sample Project
https://github.com/pot8os/Kotlin-Espresso-sample